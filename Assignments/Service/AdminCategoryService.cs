﻿using Assignments.Interface;
using System;
using Assignments.Models;
using System.Data.SqlClient;
using System.Data;
using System.Linq;

namespace Assignments.Service
{
    public class AdminCategoryService : IAdminCategoryInterface
    {
        assignment1Entities2 db = new assignment1Entities2();
        private SqlConnection con;

        public void GetConnString()
        {
            string str = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            con = new SqlConnection(str);
        }
        public void Add_Category(Category category)
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("Add_Category", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategoryName", category.CategoryName);
            cmd.Parameters.AddWithValue("@CreatedBy", category.CreatedBy);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public DataSet Show_record_byid(int categoryId)
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("Category_ById", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategorId", categoryId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public void Update_Category(Category category)
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("UpdateCategory", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategoryId", category.CategoryId);
            cmd.Parameters.AddWithValue("@CategoryName", category.CategoryName);
            cmd.Parameters.AddWithValue("@CreatedBy", category.CreatedBy);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void Delete_Category(int id)
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("DeleteCategory", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategoryId", id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void Pagination(string sortOn, string orderBy, string pSortOn, string keyword, int? page)
        {
            int recordsPerPage = 5;

            if (!page.HasValue)
            {
                page = 1; // set initial page value
                if (string.IsNullOrWhiteSpace(orderBy) || orderBy.Equals("asc"))
                {
                    orderBy = "desc";
                }
                else
                {
                    orderBy = "asc";
                }
            }

            // override the sort order if the previous sort order and current request sort order is different
            if (!string.IsNullOrWhiteSpace(sortOn) && !sortOn.Equals(pSortOn,
StringComparison.CurrentCultureIgnoreCase))
            {
                orderBy = "asc";
            }

            //ViewBag.OrderBy = orderBy;
            //ViewBag.SortOn = sortOn;
            //ViewBag.Keyword = keyword;

            var list = db.Categories.AsQueryable();

            switch (sortOn)
            {
                case "CategoryName":
                    if (orderBy.Equals("desc"))
                    {
                        list = list.OrderByDescending(p => p.CategoryName);
                    }
                    else
                    {
                        list = list.OrderBy(p => p.CategoryName);
                    }
                    break;
                default:
                    list = list.OrderBy(p => p.CategoryId);
                    break;
            }
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                list = list.Where(f => f.CategoryName.StartsWith(keyword));
            }
        }
    }
}