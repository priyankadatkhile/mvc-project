﻿using System;
using Assignments.Interface;
using System.Data.SqlClient;
using Assignments.Models;
using System.Data;
using System.Collections.Generic;
using Assignments.Database;
using System.Web.Mvc;

namespace Assignments.Service
{
    public class RegisterService : IRegister
    {
        assignment1Entities2 db = new assignment1Entities2();
        private SqlConnection con;
        public void GetConnString()
        {
            string str = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            con = new SqlConnection(str);
        }

        public void Register(Models.Registration registration)
        {
            DateTime date = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            GetConnString();
            con.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("InsertUsers", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", registration.UserId);
                cmd.Parameters.AddWithValue("@Name", registration.Name);
                cmd.Parameters.AddWithValue("@username", registration.username);
                cmd.Parameters.AddWithValue("@Pass", registration.Pass);
                cmd.Parameters.AddWithValue("@Email", registration.Email);
                cmd.Parameters.AddWithValue("@number", registration.number);
                cmd.Parameters.AddWithValue("@createdDate", date);
                cmd.Parameters.AddWithValue("@RoleId", registration.RoleName);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet GetAllRols()
        {
            GetConnString();
            DataSet ds = null;
            try
            {
                SqlCommand cmd = new SqlCommand("GetRoles", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                return ds;
            }
            return ds;
        }

        public DataSet Login(Login user)
        {
            GetConnString();
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("GetUsers", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@username", user.username);
                cmd.Parameters.AddWithValue("@Pass", user.pass);
                con.Open();
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable(); ;
                sda.Fill(ds);
                cmd.ExecuteNonQuery();
                con.Close();

            }
            catch (Exception)
            {
            }
            return ds;
        }
    }
}