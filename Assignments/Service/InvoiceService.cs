﻿using Assignments.Interface;
using System.Collections.Generic;
using System.Linq;
using Assignments.Models;

namespace Assignments.Service
{
    public class InvoiceService : Iinvoice
    {
        assignment1Entities2 db = new assignment1Entities2();
        public List<InvoiceModel> Getinvoice()
        {
            return (from u in db.Users
                    join i in db.Invoices on u.UserId equals i.in_fk_user
                    join o in db.Orders on i.in_id equals o.o_fk_invoice
                    join p in db.Products on o.o_fk_pro equals p.ProductId
                    //where o.o_id = 
                    select new InvoiceModel
                    {
                        userName = u.Name,
                        productName = p.ProductName,
                        description = p.Description,
                        in_totalbill = i.in_totalbill,
                        o_bill = o.o_bill,
                        quantity = o.o_qty
                    }).ToList();
        }

        public int GetCurrentUserId()
        {
            string userName = "";
            int uid = 0;
            if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.User != null
                      && System.Web.HttpContext.Current.User.Identity.Name != null)
            {
                userName = System.Web.HttpContext.Current.User.Identity.Name;
            }

            uid = db.Users.AsNoTracking().Where(x => x.username == userName).Select(x => x.UserId).FirstOrDefault();

            return uid;
        }
    }
}