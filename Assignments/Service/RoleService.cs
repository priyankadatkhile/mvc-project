﻿using Assignments.Interface;
using System.Data;
using System.Data.SqlClient;
using System;
using Assignments.Models;

namespace Assignments.Service
{
    public class RoleService : IRole
    {
        private SqlConnection con;

        public void GetConnString()
        {
            string str = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            con = new SqlConnection(str);
        }

        public void Add_Role(Role role)
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("Add_Role", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleName", role.RoleName);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public DataSet Show_Data()
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("Role_All", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public void update_Role(Role role)
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("UpdateRole", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleID", role.RoleId);
            cmd.Parameters.AddWithValue("@RoleName", role.RoleName);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public DataSet Show_role_byid(int RoleId)
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("Role_ById", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleId", RoleId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public void Delete_Role(int RoleId)
        {
            GetConnString();
            SqlCommand cmd = new SqlCommand("DeleteRoles", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleId", RoleId);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}