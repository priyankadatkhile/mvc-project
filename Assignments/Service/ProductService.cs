﻿using Assignments.Interface;
using System;
using System.Data;
using Assignments.Models;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;

namespace Assignments.Service
{
    public class ProductService : IProduct
    {
        assignment1Entities2 db = new assignment1Entities2();
        private SqlConnection con;
        public void GetConnString()
        {
            string str = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            con = new SqlConnection(str);
        }

        public DataSet GetAllCategory()
        {
            GetConnString();
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllCategories", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                return ds;
            }
            return ds;
        }

        public List<Category1> SelectallCategories()
        {
            GetConnString();
            DataSet ds = null;
            List<Category1> categorylist = null;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllCategories", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                categorylist = new List<Category1>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Category1 cobj = new Category1();
                    cobj.CategoryId = Convert.ToInt32(ds.Tables[0].Rows[i]["CategoryId"].ToString());
                    cobj.CategoryName = ds.Tables[0].Rows[i]["CategoryName"].ToString();
                    categorylist.Add(cobj);
                }
                return categorylist;
            }
            catch
            {
                return categorylist;
            }
            finally
            {
                con.Close();
            }
        }

        public string Create(HttpPostedFileBase file, Product product)
        {
            string result = "";
            string filename = Path.GetFileName(file.FileName);
            string _filename = DateTime.Now.ToString("yymmssff") + filename;
            string extension = Path.GetExtension(file.FileName);
            string path = Path.Combine(HttpContext.Current.Server.MapPath("~/image/"), _filename);
            product.ProductImage = "~/image/" + _filename;

            if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
            {
                if (file.ContentLength <= 100000)
                {
                    product.CategoryId = Convert.ToInt32(product.CategoryName);
                    db.Products.Add(product);
                    if (db.SaveChanges() > 0)
                    {
                        file.SaveAs(path);
                        result = "Product added";
                        System.Web.HttpContext.Current.Response.Redirect("~/Supervisor/Index");
                        //HttpContext.Current.Response.Redirect("~/Supervisor/Index");
                        //Response.Redirect(Url.Action("Index", "Supervisor"));
                    }
                }
                else
                {
                    //dynamic ViewBag = filterContext.Controller.ViewBag;
                    //ViewBag.msg = "Field size must be equal or less than 1mb";
                    result = "Field size must be equal or less than 1mb";
                }
            }
            else
            {
                //dynamic ViewBag = filterContext.Controller.ViewBag;
                //ViewBag.msg = "Invalid File Type";
                //Console.Write("Invalid File Type");
                result = "Invalid File Type";
            }
            return result;
        }

        public string Edit(HttpPostedFileBase file, Product product)
        {
            string result = "";
            string filename = Path.GetFileName(file.FileName);
            string _filename = DateTime.Now.ToString("yymmssff") + filename;

            string extension = Path.GetExtension(file.FileName);

            string path = Path.Combine(HttpContext.Current.Server.MapPath("~/image/"), _filename);

            product.ProductImage = "~/image/" + _filename;

            if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".png")
            {
                if (file.ContentLength <= 100000)
                {
                    product.CategoryId = Convert.ToInt32(product.CategoryName);

                    db.Entry(product).State = EntityState.Modified;

                    string oldimgPath = HttpContext.Current.Request.MapPath(HttpContext.Current.Session["imgPath"].ToString());

                    if (db.SaveChanges() > 0)
                    {
                        file.SaveAs(path);
                        // To Delete old image from folder
                        if (System.IO.File.Exists(oldimgPath))
                        {
                            System.IO.File.Delete(oldimgPath);
                        }
                        result = "Data Updated";
                        System.Web.HttpContext.Current.Response.Redirect("~/Supervisor/Index");
                        //return RedirectToAction("index");
                    }
                }
                else
                {
                    result = "Field size must be equal or less than 1mb";
                    //ViewBag.msg = "Field size must be equal or less than 1mb";
                }
            }
            else
            {
                result = "Invalid File Type";
                //ViewBag.msg = "Invalid File Type";
            }
            return result;
        }

        public string Delete(int? id)
        {
            string result = "";

            if (id == null)
            {
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                HttpStatusCodeResult coderesult = new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                return coderesult.ToString();
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = db.Products.Find(id);
            if (product == null)
            {
                HttpNotFoundResult foundResult = new HttpNotFoundResult();
                return foundResult.StatusCode.ToString();
            }

            string currentImage = HttpContext.Current.Request.MapPath(product.ProductImage);

            db.Entry(product).State = EntityState.Deleted;
            if (db.SaveChanges() > 0)
            {
                if (System.IO.File.Exists(currentImage))
                {
                    System.IO.File.Delete(currentImage);
                }
                result = "Data Deleted";
                //TempData["msg"] = "Data Deleted";
                //return RedirectToAction("index");
            }
            return result;
        }
    }
}
