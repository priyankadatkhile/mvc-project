﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Assignments.Models;

namespace Assignments.Database
{
    public class Product
    {
        private SqlConnection con;
        private void Connect()
        {

            string str = @"Data Source =.; Initial Catalog = assignment1; Integrated Security = True; MultipleActiveResultSets = True; Application Name = EntityFramework";
            //string Constring = ConfigurationManager.ConnectionStrings["assignment1Entities"].ToString();
            con = new SqlConnection(str);
        }

        public DataSet GetAllCategory()
        {
            Connect();
            DataSet ds = new DataSet();
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllCategories", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                return ds;
            }
            return ds;
        }

        public List<Category1> SelectallCategories()
        {
            Connect();
            // SqlConnection con = null;
            DataSet ds = null;
            List<Category1> categorylist = null;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllCategories", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                categorylist = new List<Category1>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Category1 cobj = new Category1();
                    cobj.CategoryId = Convert.ToInt32(ds.Tables[0].Rows[i]["CategoryId"].ToString());
                    cobj.CategoryName = ds.Tables[0].Rows[i]["CategoryName"].ToString();
                    categorylist.Add(cobj);
                }
                return categorylist;
            }
            catch
            {
                return categorylist;
            }
            finally
            {
                con.Close();
            }
        }

    }
}