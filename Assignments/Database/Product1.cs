﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Assignments.Database
{
    public class Product1
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public int CategoryId { get; set; }

        public string Description { get; set; }

        public HttpPostedFileBase ImagePath { get; set; }
        public decimal ProductPrice { get; set; }

        public int UserId { get; set; }
        public IEnumerable<SelectListItem> CategorySelectListItem { get; set; }

        //[DisplayName("Upload File")]
        //public string imagePath { get; set; }


        //public string CategoryName { get; set; }
        //public string CreatedBy { get; set; }

        //public HttpPostedFileBase ImageFile { get; set; }

        public DataSet GetCategory { get; set; }
    }
}