﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace Assignments.Database
{
    public class Registration
    {

        private SqlConnection con;
        private void Connect()
        {
            string str = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            //string Constring = ConfigurationManager.ConnectionStrings["assignment1Entities"].ToString();
            con = new SqlConnection(str);
        }

        public DataSet GetAllRols()
        {
            Connect();
            DataSet ds = null;
            try
            {
                SqlCommand cmd = new SqlCommand("GetRoles", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                return ds;
            }
            return ds;
        }

    }
}