﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Assignments.Models;

namespace Assignments.Database
{
    public class Role
    {
        private SqlConnection con;
        private void Connect()
        {
            string Constring = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            con = new SqlConnection(Constring);
        }
        public void Add_record(Models.Role role)
        {
            Connect();
            SqlCommand cmd = new SqlCommand("Add_Role", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleName", role.RoleName);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void update_record(Models.Role1 role)
        {
            Connect();
            SqlCommand cmd = new SqlCommand("UpdateRole", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleID", role.RoleId);
            cmd.Parameters.AddWithValue("@RoleName", role.RoleName);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public DataSet Show_record_byid(int RoleId)
        {
            Connect();
            SqlCommand cmd = new SqlCommand("Role_ById", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleId", RoleId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public DataSet Show_Data()
        {
            Connect();
            SqlCommand cmd = new SqlCommand("Role_All", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public void delete_record(int id)
        {
            Connect();
            SqlCommand cmd = new SqlCommand("DeleteRoles", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@RoleId", id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}
