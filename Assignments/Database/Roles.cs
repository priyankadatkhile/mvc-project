﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Assignments.Models;

namespace Assignments.Database
{
    public class Roles
    {
        int ID = 0;
        string RolesName = "";
        private SqlConnection con;
        private void Connect()
        {
            string Constring = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            con = new SqlConnection(Constring);
        }

        public bool Save(Role1 obj)
        {
            Connect();
            SqlCommand cmd = new SqlCommand("IURole", con);
            cmd.CommandType = CommandType.StoredProcedure;

            if (obj.RoleName == null || obj.RoleName == "")
            {
                cmd.Parameters.AddWithValue("@RoleName", 0);
            }
            else
            {
                cmd.Parameters.AddWithValue("@RoleName", obj.RoleName);
            }

            if (obj.RoleId == 0)
            {
                cmd.Parameters.AddWithValue("@RoleID", 0);
                cmd.Parameters.AddWithValue("@Mode", "Add");
            }
            else
            {
                cmd.Parameters.AddWithValue("@RoleID", obj.RoleId);
                cmd.Parameters.AddWithValue("@Mode", "Edit");
            }
            //  cmd.Parameters.AddWithValue("@Mode", "Add");
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public DataSet SelectAllDataRoles()
        {
            Connect();
            DataSet ds = null;
            try
            {
                SqlCommand cmd = new SqlCommand("GetAllRoles", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RolesID", ID);
                cmd.Parameters.AddWithValue("@RolesName", RolesName);
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;
                ds = new DataSet();
                da.Fill(ds);
                con.Close();
                return ds;
            }
            catch (Exception)
            {
                return ds;
            }
        }

        public string Delete(int RoleId)
        {
            string Table = string.Empty;
            try
            {
                Connect();
                SqlCommand cmd = new SqlCommand("DeleteRoles", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@RoleId", RoleId);

                con.Open();
                int i = cmd.ExecuteNonQuery();

                con.Close();
                if (i > 0)
                {
                    return Table;
                }
                else
                {
                    return Table;
                }
            }
            catch (Exception)
            {
                return Table;
            }
        }
    }
}