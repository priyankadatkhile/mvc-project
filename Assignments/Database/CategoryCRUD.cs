﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using Assignments.Models;

namespace Assignments.Database
{
    public class CategoryCRUD
    {
        SqlConnection con = new SqlConnection("Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework");

        public void Add_record(Models.Category cat)
        {
            SqlCommand cmd = new SqlCommand("Add_Category", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategoryName", cat.CategoryName);
            cmd.Parameters.AddWithValue("@CreatedBy", cat.CreatedBy);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public void update_record(Models.Category cat)
        {
            SqlCommand cmd = new SqlCommand("UpdateCategory", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategoryId", cat.CategoryId);
            cmd.Parameters.AddWithValue("@CategoryName", cat.CategoryName);
            cmd.Parameters.AddWithValue("@CreatedBy", cat.CreatedBy);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }

        public DataSet Show_record_byid(int categoryId)
        {
            SqlCommand cmd = new SqlCommand("Category_ById", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategorId", categoryId);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }

        public DataSet Show_Data()
        {
            SqlCommand cmd = new SqlCommand("Category_All", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;
        }


        public void delete_record(int id)
        {
            SqlCommand cmd = new SqlCommand("DeleteCategory", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@CategoryId", id);
            con.Open();
            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}