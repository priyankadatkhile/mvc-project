﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Assignments.Database;
using System.Data;
using System.Web.Mvc;
//using System.Data.SqlClient;

namespace Assignments.Database
{
    public class Category
    {
        int UserID = Convert.ToInt32(HttpContext.Current.Session["UserID"]);

        private SqlConnection con;
        private void Connect()
        {
            string Constring = @"Data Source=.;Initial Catalog=assignment1;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework";
            con = new SqlConnection(Constring);
        }

        public bool CheckCategory(int CategoryID, string CategoryName)
        {
            Connect();
            string Table;
            bool flag;
            try
            {
                SqlCommand cmd = new SqlCommand("CheckCategory", con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CategoryId", CategoryID);
                cmd.Parameters.AddWithValue("@CategoryName", CategoryName.ToUpper());
                cmd.Parameters.Add("@NameExists", SqlDbType.NVarChar, 50);
                cmd.Parameters["@NameExists"].Direction = ParameterDirection.Output;
                con.Open();
                int i = cmd.ExecuteNonQuery();
                Table = (string)cmd.Parameters["@NameExists"].Value;
                if (Convert.ToInt32(Table) == 1)
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                }
                return flag;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Categories(Models.Category obj)
        {
            Connect();

            SqlCommand cmd = new SqlCommand("IUCategory", con);

            cmd.CommandType = CommandType.StoredProcedure;

            if (obj.CategoryId == 0)
            {

                cmd.Parameters.AddWithValue("@CategoryID", 0);
                cmd.Parameters.AddWithValue("@Mode", "Add");
            }
            else
            {
                cmd.Parameters.AddWithValue("@CategoryID", obj.CategoryId);
                cmd.Parameters.AddWithValue("@Mode", "Edit");
            }

            //cmd.Parameters.AddWithValue("@CategoryID", 0);

            //cmd.Parameters.AddWithValue("@CreationID", UserID);

            //cmd.Parameters.AddWithValue("@Mode", "Add");
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();
            if (i > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}