﻿using System.Web.Mvc;
using System.Data;
using Assignments.Models;
using System.Web.Security;
using Assignments.Interface;
using System.Security.Claims;

namespace Assignments.Controllers
{
    public class LoginController : Controller
    {
        private readonly IRegister _register;

        public LoginController(IRegister register)
        {
            _register = register;
        }

        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login user,string returnUrl)
        {
            DataSet ds = new DataSet();
            ds = _register.Login(user);

            if (ds.Tables.Count > 0)
            {
                //if (ds.Tables[0].Rows[0]["RoleName"].ToString() == "Administration")
                //{
                //    FormsAuthentication.SetAuthCookie(user.username, false);
                //    return RedirectToAction("Index", "Admin");
                //}
                //else if (ds.Tables[0].Rows[0]["RoleName"].ToString() == "Supervisor")
                //{
                //    FormsAuthentication.SetAuthCookie(user.username, false);
                //    return RedirectToAction("Index", "Supervisor");
                //}
                //else
                //{
                //    FormsAuthentication.SetAuthCookie(user.username, false);
                //    return RedirectToAction("Index", "User");
                //}

                var rolename = ds.Tables[0].Rows[0]["RoleName"].ToString();

                JwtTokenService jwttoken = new JwtTokenService();
                string token = jwttoken.GenerateToken(user.username);
                //string token = jwttoken.GenerateToken(rolename);

                var claimsPrincipal = jwttoken.GetPrincipal(token);

                if(claimsPrincipal.HasClaim(x=>x.Type == ClaimTypes.Role && x.Value == user.RoleName) && claimsPrincipal.HasClaim(x=>x.Type==ClaimTypes.Name && x.Value == user.username))
                {
                    return RedirectToAction("Index", "Admin");
                }

                //foreach(var r in user.RoleName)
                //{
                //    if(claimsPrincipal.HasClaim(x=>x.Type==ClaimTypes.Role && x.Value==r.))
                //}
            }
            else
                ModelState.AddModelError(string.Empty, "The user name or Password is incorrect");
            return View("Login");
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();  // it will clear the session at the end of request
            return RedirectToAction("Login");
        } 
    }
}