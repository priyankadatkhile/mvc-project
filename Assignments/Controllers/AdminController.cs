﻿using System;
using System.Linq;
using System.Web.Mvc;
using Assignments.Models;
using System.Data;
using PagedList;
using Assignments.Interface;

namespace Assignments.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly Iinvoice _Iinvoice;
        private readonly IAdminCategoryInterface _IAdminCategoryInterface;

        public AdminController(Iinvoice invoice, IAdminCategoryInterface adminCategoryInterface)
        {
            _Iinvoice = invoice;
            _IAdminCategoryInterface = adminCategoryInterface;
        }

        assignment1Entities2 db = new assignment1Entities2();

        // GET: /Admin/
        public ActionResult Index(string sortOn, string orderBy, string pSortOn, string keyword, int? page)
        {
            int recordsPerPage = 5;
            if (!page.HasValue)
            {
                page = 1; // set initial page value
                if (string.IsNullOrWhiteSpace(orderBy) || orderBy.Equals("asc"))
                {
                    orderBy = "desc";
                }
                else
                {
                    orderBy = "asc";
                }
            }

            // override the sort order if the previous sort order and current request sort order is different
            if (!string.IsNullOrWhiteSpace(sortOn) && !sortOn.Equals(pSortOn,
StringComparison.CurrentCultureIgnoreCase))
            {
                orderBy = "asc";
            }

            ViewBag.OrderBy = orderBy;
            ViewBag.SortOn = sortOn;
            ViewBag.Keyword = keyword;

            var list = db.Categories.AsQueryable();

            switch (sortOn)
            {
                case "CategoryName":
                    if (orderBy.Equals("desc"))
                    {
                        list = list.OrderByDescending(p => p.CategoryName);
                    }
                    else
                    {
                        list = list.OrderBy(p => p.CategoryName);
                    }
                    break;
                default:
                    list = list.OrderBy(p => p.CategoryId);
                    break;
            }
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                list = list.Where(f => f.CategoryName.StartsWith(keyword));
            }
            var finalList = list.ToPagedList(page.Value, recordsPerPage);
            return View(finalList);
        }

        //[HttpGet]
        public ActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCategory(FormCollection fc, Models.Category category)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Models.Category category1 = new Models.Category();
                    category1.CategoryName = fc["CategoryName"];

                    int userId = _Iinvoice.GetCurrentUserId();
                    category1.CreatedBy = userId;

                    _IAdminCategoryInterface.Add_Category(category1);
                    TempData["msg"] = "Inserted";
                    return RedirectToAction("Index", "Admin");
                }
                return View();
            }
            catch
            {
                return View();
            }
        }
        public ActionResult UpdateCategory(int categoryId)
        {
            DataSet ds = _IAdminCategoryInterface.Show_record_byid(categoryId);
            ViewBag.categoryRecord = ds.Tables[0];
            return View();
        }

        [HttpPost]
        public ActionResult UpdateCategory(int categoryId, FormCollection fc)
        {
            Models.Category category = new Models.Category();
            category.CategoryId = categoryId;
            category.CategoryName = fc["CategoryName"];

            int userId = _Iinvoice.GetCurrentUserId();
            category.CreatedBy = userId;

            _IAdminCategoryInterface.Update_Category(category);
            TempData["msg"] = "Updated";
            return RedirectToAction("Index");
        }

        public ActionResult DeleteCategory(int categoryId)
        {
            _IAdminCategoryInterface.Delete_Category(categoryId);
            TempData["msg"] = "Deleted";
            return RedirectToAction("Index");
        }
    }
}