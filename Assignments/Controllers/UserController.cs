﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Assignments.Models;
using Assignments.Interface;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Text;

namespace Assignments.Controllers
{
    public class UserController : Controller
    {
        private readonly Iinvoice _invoice;
        private readonly IUser _user;
        public UserController(Iinvoice invoice,IUser user)
        {
            _invoice = invoice;
            _user = user;
        }

        assignment1Entities2 db = new assignment1Entities2();
        // GET: User

        public ActionResult Index()
        {
            // Sum of bill
            if (TempData["cart"] != null)
            {
                float x = 0;
                List<cart> li2 = TempData["cart"] as List<cart>;
                foreach (var item in li2)
                {
                    x += item.bill;
                }
                TempData["total"] = x;
            }

            TempData.Keep();
            // display list from current uploads
            return View(db.Products.OrderByDescending(x => x.ProductId).ToList());
        }

        [HttpGet]
        public ActionResult AddToCart(int? Id)
        {
            // Find Product
            Product p = _user.AddToCart(Id);
            return View(p);
        }

        List<cart> li = new List<cart>();
        [HttpPost]
        public ActionResult AddToCart(Product product, string qty, int Id)
        {
            Product p = db.Products.Where(a => a.ProductId == Id).SingleOrDefault();

            cart c = new cart();
            c.ProductId = p.ProductId;
            c.ProductPrice = (float)p.ProductPrice;
            c.ProductName = p.ProductName;
            c.qty = Convert.ToInt32(qty);
            c.bill = c.ProductPrice * c.qty;

            // if cart is blank
            if (TempData["cart"] == null)
            {
                li.Add(c);
                TempData["cart"] = li;
            }
            else
            {
                List<cart> li2 = TempData["cart"] as List<cart>;        // For Existing list
                int flag = 0;
                foreach (var item in li2)
                {
                    if (item.ProductId == c.ProductId)
                    {
                        item.qty += c.qty;
                        item.bill += c.bill;
                        flag = 1;
                    }
                }
                if (flag == 0)
                {
                    // for new item
                    li2.Add(c);
                }
                TempData["cart"] = li2;
            }

            //TempData["cart"] = li;
            TempData.Keep();

            return RedirectToAction("Index");
        }

        public ActionResult remove(int? id)
        {
            List<cart> li2 = TempData["cart"] as List<cart>;
            cart c = li2.Where(x => x.ProductId == id).SingleOrDefault();
            li2.Remove(c);
            float h = 0;
            foreach (var item in li2)
            {
                h += item.bill;
            }
            TempData["total"] = h;
            return RedirectToAction("checkout");
        }

        [Authorize]
        public ActionResult checkout()
        {
            TempData.Keep();
            return View();
        }

        [HttpPost]
        public ActionResult checkout(Order O)
        {
            List<cart> li = TempData["cart"] as List<cart>;
            int userId = _invoice.GetCurrentUserId();

            Invoice iv = new Invoice();
            iv.in_fk_user = userId;
            iv.in_date = System.DateTime.Now;
            iv.in_totalbill = (float)TempData["Total"];
            db.Invoices.Add(iv);
            db.SaveChanges();

            // foreach loop for multiple order

            foreach (var item in li)
            {
                Order od = new Order();
                od.o_fk_pro = item.ProductId;
                od.o_fk_invoice = iv.in_id;
                od.o_date = System.DateTime.Now;
                od.o_qty = item.qty;
                od.o_unitprice = (int)item.ProductPrice;
                od.o_bill = item.bill;
                db.Orders.Add(od);
                db.SaveChanges();
            }

            TempData.Remove("total");
            TempData.Remove("cart");

            TempData["msg"] = "Transaction Completed....";
            TempData.Keep();
            return RedirectToAction("Index");
        }

        public ActionResult Invoice()
        {
            var result = _invoice.Getinvoice();
            return View(result);
        }

        public FileResult CreatePdf()
        {
            MemoryStream workStream = new MemoryStream();
            StringBuilder status = new StringBuilder("");
            DateTime dTime = DateTime.Now;
            // file name to be created 
            string strPDFFileName = string.Format("SamplePdf" + dTime.ToString("yyyyMMdd") + "-" + ".pdf");
            Document doc = new Document();
            doc.SetMargins(0f, 0f, 0f, 0f);
            // Create PDF Table with 5 coulmns
            PdfPTable tableLayout = new PdfPTable(6);
            doc.SetMargins(0f, 0f, 0f, 0f);

            // Create PDF Table

            // file will created in this path
            string strAttachment = Server.MapPath("~/Downloads" + strPDFFileName);

            PdfWriter.GetInstance(doc, workStream).CloseStream = false;
            doc.Open();

            //Add content to PDF
            doc.Add(Add_Content_To_PDF(tableLayout));

            // closing the document
            doc.Close();

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;

            return File(workStream, "application/pdf", strPDFFileName);
        }

        private PdfPTable Add_Content_To_PDF(PdfPTable tableLayout)
        {
            float[] headers = { 50, 24, 45, 35, 24,30 };   // header widths
            //float[] headers = { 50, 45, 24 };   // header widths
            tableLayout.SetWidths(headers);             // Set the pdf headers
            tableLayout.WidthPercentage = 100;
            tableLayout.HeaderRows = 1;
            // Add Title to the PDF file at the Top

            //List<Product> product = db.Products.ToList<Product>();
            var invoice = _invoice.Getinvoice();

            tableLayout.AddCell(new PdfPCell(new Phrase("Order Placed Invoice Bill", new Font(Font.FontFamily.HELVETICA, 8, 1,
                new iTextSharp.text.BaseColor(0, 0, 0))))
            { Colspan = 12, Border = 0, PaddingBottom = 5, HorizontalAlignment = Element.ALIGN_CENTER });

            // Add Header

            AddCellToHeader(tableLayout, "UserName");
            AddCellToHeader(tableLayout, "ProductName");
            AddCellToHeader(tableLayout, "Description");
            AddCellToHeader(tableLayout, "Quantity");
            AddCellToHeader(tableLayout, "o_bill");
            AddCellToHeader(tableLayout, "in_totalbill");

            //// Add Body

            foreach (var pro in invoice)
            {
                AddCellToBody(tableLayout, pro.userName);
                AddCellToBody(tableLayout, pro.productName);
                AddCellToBody(tableLayout, pro.description);
                AddCellToBody(tableLayout, pro.quantity.ToString());
                AddCellToBody(tableLayout, pro.o_bill.ToString());
                AddCellToBody(tableLayout, pro.in_totalbill.ToString());
            }
            return tableLayout;
        }


        // Method to add single cell to the Header
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.YELLOW))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new iTextSharp.text.BaseColor(128, 0, 0) });
        }

        // Method to add single cell to the body
        private static void AddCellToBody(PdfPTable tableLayout, string cellText)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 8, 1, iTextSharp.text.BaseColor.BLACK))) { HorizontalAlignment = Element.ALIGN_LEFT, Padding = 5, BackgroundColor = new iTextSharp.text.BaseColor(255, 255, 255) });
        }
    }
}