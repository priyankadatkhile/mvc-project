﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assignments.Models;
using System.Data;
using System.Net;
using System.Data.Entity;
using PagedList;
using Assignments.Interface;

namespace Assignments.Controllers
{
    [Authorize]
    public class SupervisorController : Controller
    {
        private readonly Iinvoice _Iinvoice;
        private readonly IProduct _IProduct;

        public SupervisorController(Iinvoice invoice, IProduct product)
        {
            _Iinvoice = invoice;
            _IProduct = product;
        }

        assignment1Entities2 db = new assignment1Entities2();
        Assignments.Models.Product product = new Models.Product();

        public ActionResult Index(string sortOn, string orderBy, string pSortOn, string keyword, int? page)
        {
            int recordsPerPage = 5;
            if (!page.HasValue)
            {
                page = 1; // set initial page value
                if (string.IsNullOrWhiteSpace(orderBy) || orderBy.Equals("asc"))
                {
                    orderBy = "desc";
                }
                else
                {
                    orderBy = "asc";
                }
            }

            // override the sort order if the previous sort order and current request sort order is different
            if (!string.IsNullOrWhiteSpace(sortOn) && !sortOn.Equals(pSortOn,
StringComparison.CurrentCultureIgnoreCase))
            {
                orderBy = "asc";
            }

            ViewBag.OrderBy = orderBy;
            ViewBag.SortOn = sortOn;
            ViewBag.Keyword = keyword;

            var list = db.Products.AsQueryable();

            switch (sortOn)
            {
                case "ProductName":
                    if (orderBy.Equals("desc"))
                    {
                        list = list.OrderByDescending(p => p.ProductName);
                    }
                    else
                    {
                        list = list.OrderBy(p => p.ProductName);
                    }
                    break;
                case "ProductPrice":
                    if (orderBy.Equals("desc"))
                    {
                        list = list.OrderByDescending(p => p.ProductPrice);
                    }
                    else
                    {
                        list = list.OrderBy(p => p.ProductPrice);
                    }
                    break;
                default:
                    list = list.OrderBy(p => p.ProductId);
                    break;
            }
            if (!string.IsNullOrWhiteSpace(keyword))
            {
                list = list.Where(f => f.ProductName.StartsWith(keyword));
            }
            var finalList = list.ToPagedList(page.Value, recordsPerPage);
            return View(finalList);
        }

        [HttpGet]
        public ActionResult Create()
        {
            product.GetCategory = _IProduct.GetAllCategory();
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(HttpPostedFileBase file, Models.Product product)
        {
            if (ModelState.IsValid)
            {
                int userId = _Iinvoice.GetCurrentUserId();
                product.CreatedBy = userId;
                string result = _IProduct.Create(file, product);
                ViewBag.msg = result;
            }
            return View();
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var product = db.Products.Find(id);
            Session["imgPath"] = product.ProductImage;

            var catlist = _IProduct.SelectallCategories();
            ViewBag.catlist = new SelectList(catlist, "CategoryId", "CategoryName", product.CategoryId);

            // if record not found
            if (product == null)
            {
                return HttpNotFound();      // 404 Page display
            }
            return View(product);
        }

        [HttpPost]
        public ActionResult Edit(HttpPostedFileBase file, Models.Product product)
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    int userId = _Iinvoice.GetCurrentUserId();
                    product.CreatedBy = userId;

                    string result = _IProduct.Edit(file, product);
                    ViewBag.msg = result;
                }
                else
                {
                    product.ProductImage = Session["imgPath"].ToString();
                    db.Entry(product).State = EntityState.Modified;
                    product.CategoryId = Convert.ToInt32(product.CategoryName);

                    if (db.SaveChanges() > 0)
                    {
                        TempData["msg"] = "Data Updated";
                        return RedirectToAction("index");
                    }
                }
            }
            return View();
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }

            string currentImage = Request.MapPath(product.ProductImage);

            db.Entry(product).State = EntityState.Deleted;
            if (db.SaveChanges() > 0)
            {
                if (System.IO.File.Exists(currentImage))
                {
                    System.IO.File.Delete(currentImage);
                }
                TempData["msg"] = "Data Deleted";
                return RedirectToAction("index");
            }
            return View();
            //string result = _IProduct.Delete(id);
            //ViewBag.msg = result;

            //return View("Index");
        }
    }
}