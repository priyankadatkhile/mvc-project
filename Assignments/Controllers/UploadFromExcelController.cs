﻿using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LinqToExcel;
using Assignments.Models;

namespace Assignments.Controllers
{
    public class UploadFromExcelController : Controller
    {
        private assignment1Entities2 db = new assignment1Entities2();

        // GET: UploadFromExcel
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UploadExcel(Models.Product product, HttpPostedFileBase FileUpload)
        {
            List<string> data = new List<string>();
            if (FileUpload != null)
            {
                // Check File type it is excel or spreadsheet
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {
                    string filename = FileUpload.FileName;
                    string targetpath = Server.MapPath("~/Data/");
                    FileUpload.SaveAs(targetpath + filename);
                    string pathToExcelFile = targetpath + filename;
                    var connectionString = "";
                    if (filename.EndsWith(".xls"))
                    {
                        connectionString = string.Format(@"Provider=Microsoft.Jet.OLEDB.8.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                    }
                    else if (filename.EndsWith(".xlsx"))
                    {
                        connectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=""Excel 12.0 Xml;HDR=YES;""", pathToExcelFile);
                    }

                    var adapter = new OleDbDataAdapter("Select * from [Sheet1$]", connectionString);
                    var ds = new DataSet();

                    adapter.Fill(ds, "ExcelTable");

                    DataTable dtable = ds.Tables["ExcelTable"];

                    string sheetname = "Sheet1";

                    var excelFile = new ExcelQueryFactory(pathToExcelFile);
                    var productalbum = from a in excelFile.Worksheet<Models.Product>(sheetname) select a;

                    foreach (var a in productalbum)
                    {
                        try
                        {
                            if (a.ProductName != "" && a.Description != "" && a.ProductPrice != 0)
                            {
                                Models.Product product1 = new Models.Product();
                                product1.CategoryId = a.ProductId;
                                product1.ProductName = a.ProductName;
                                product1.Description = a.Description;
                                product1.ProductImage = a.ProductImage;
                                product1.ProductPrice = a.ProductPrice;
                                db.Products.Add(product1);

                                db.SaveChanges();
                            }
                            else
                            {
                                data.Add("<ul>");
                                if (a.ProductName == "" || a.ProductName == null)
                                    data.Add("<li> Product Name is required</li>");
                                if(a.Description == "" || a.Description == null)
                                    data.Add("<li> Description is required</li>");
                                if (a.ProductPrice == 0)
                                    data.Add("<li> Product Price is required</li>");
                                data.Add("</ul>");
                                data.ToArray();
                                return Json(data, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch(DbEntityValidationException ex)
                        {
                            foreach(var entityValidationErrors in ex.EntityValidationErrors)
                            {
                                foreach(var validationError in entityValidationErrors.ValidationErrors)
                                {
                                    Response.Write("Property: " + validationError.PropertyName + " Error:" + validationError.ErrorMessage);
                                }
                            }
                        }
                    }
                    // deleting excel file from folder
                    if((System.IO.File.Exists(pathToExcelFile)))
                    {
                        System.IO.File.Delete(pathToExcelFile);
                    }
                    return Json("success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    // alert message for invalid file format
                    data.Add("<ul");
                    data.Add("<li> Only Excel file format is allowed</li>");
                    data.Add("</ul>");
                    data.ToArray();
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                data.Add("</ul>");
                if (FileUpload == null)
                    data.Add("</li>Please choose Excel file</li>");
                data.Add("</ul>");
                data.ToArray();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }
    }
}