﻿using System;
using System.Web.Mvc;
using Assignments.Models;
using System.Data;
using Assignments.Interface;

namespace Assignments.Controllers
{
    public class RoleController : Controller
    {
        private readonly IRole _role;

        public RoleController(IRole role)
        {
            _role = role;
        }

        assignment1Entities2 db = new assignment1Entities2();
        Database.Role dblayer = new Database.Role();

        // GET: Role
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Show_data()
        {
            DataSet ds = _role.Show_Data();
            ViewBag.role = ds.Tables[0];
            return View();
        }

        //[HttpGet]
        public ActionResult AddRole()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddRole(FormCollection fc, Models.Role role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Models.Role role1 = new Models.Role();
                    role1.RoleName = fc["RoleName"];
                    _role.Add_Role(role1);
                    TempData["msg"] = "Inserted";
                    return RedirectToAction("Show_Data", "Role");
                }
                return View();
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        public ActionResult UpdateRole(int RoleId)
        {
            DataSet ds = _role.Show_role_byid(RoleId);
            ViewBag.RoleRecord = ds.Tables[0];
            return View();
        }

        [HttpPost]
        public ActionResult UpdateRole(int roleId, FormCollection fc)
        {
            Models.Role role = new Models.Role();
            role.RoleId = roleId;
            role.RoleName = fc["RoleName"];
            _role.update_Role(role);
            TempData["msg"] = "Updated";
            return RedirectToAction("Show_data");
        }

        public ActionResult DeleteRole(int roleId)
        {
            _role.Delete_Role(roleId);
            TempData["msg"] = "Deleted";
            return RedirectToAction("Show_data");
        }
    }
}