﻿using System;
using System.Web.Mvc;
using Assignments.Models;
using Assignments.Interface;

namespace Assignments.Controllers
{
    public class RegisterController : Controller
    {
        private readonly IRegister _register;

        public RegisterController(IRegister register)
        {
            _register = register;
        }

        // GET: Register
        Assignments.Models.Registration registration = new Models.Registration();
        assignment1Entities2 userdb = new assignment1Entities2();

        [HttpGet]
        public ActionResult Insertdate()
        {
            registration.GetRoles = _register.GetAllRols();
            return View(registration);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Insertdate(Assignments.Models.Registration registration)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _register.Register(registration);
                    TempData["msg"] = "Inserted";
                    return RedirectToAction("Login", "Login");
                }
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }
        }
    }
}

