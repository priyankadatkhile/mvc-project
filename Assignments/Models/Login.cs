﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Assignments.Models
{
    public class Login
    {
        public string username { get; set; }
        public string pass { get; set; }

        public string RoleName { get; set; }

        public int UserId { get; set; }

        public string Email { get; set; }

        public DataSet login { get; set; }
    }
}