﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Assignments.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class assignment1Entities2 : DbContext
    {
        public assignment1Entities2()
            : base("name=assignment1Entities2")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserRight> UserRights { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Invoice> Invoices { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
    
        public virtual ObjectResult<Add_Category_Result> Add_Category(string categoryName, Nullable<int> createdBy)
        {
            var categoryNameParameter = categoryName != null ?
                new ObjectParameter("CategoryName", categoryName) :
                new ObjectParameter("CategoryName", typeof(string));
    
            var createdByParameter = createdBy.HasValue ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Add_Category_Result>("Add_Category", categoryNameParameter, createdByParameter);
        }
    
        public virtual ObjectResult<Admin_Result> Admin(string username, string pass)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("username", username) :
                new ObjectParameter("username", typeof(string));
    
            var passParameter = pass != null ?
                new ObjectParameter("Pass", pass) :
                new ObjectParameter("Pass", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Admin_Result>("Admin", usernameParameter, passParameter);
        }
    
        public virtual ObjectResult<Category_All_Result> Category_All()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Category_All_Result>("Category_All");
        }
    
        public virtual ObjectResult<Category_ById_Result> Category_ById(Nullable<int> categorId)
        {
            var categorIdParameter = categorId.HasValue ?
                new ObjectParameter("CategorId", categorId) :
                new ObjectParameter("CategorId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Category_ById_Result>("Category_ById", categorIdParameter);
        }
    
        public virtual int CheckCategory(Nullable<int> categoryId, string categoryName, ObjectParameter nameExists)
        {
            var categoryIdParameter = categoryId.HasValue ?
                new ObjectParameter("CategoryId", categoryId) :
                new ObjectParameter("CategoryId", typeof(int));
    
            var categoryNameParameter = categoryName != null ?
                new ObjectParameter("CategoryName", categoryName) :
                new ObjectParameter("CategoryName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("CheckCategory", categoryIdParameter, categoryNameParameter, nameExists);
        }
    
        public virtual int DeleteCategory(Nullable<int> categoryId)
        {
            var categoryIdParameter = categoryId.HasValue ?
                new ObjectParameter("CategoryId", categoryId) :
                new ObjectParameter("CategoryId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteCategory", categoryIdParameter);
        }
    
        public virtual int DeleteRole(Nullable<int> roleId, string roleName, ObjectParameter tableName)
        {
            var roleIdParameter = roleId.HasValue ?
                new ObjectParameter("RoleId", roleId) :
                new ObjectParameter("RoleId", typeof(int));
    
            var roleNameParameter = roleName != null ?
                new ObjectParameter("RoleName", roleName) :
                new ObjectParameter("RoleName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteRole", roleIdParameter, roleNameParameter, tableName);
        }
    
        public virtual int DeleteRoles(Nullable<int> roleId)
        {
            var roleIdParameter = roleId.HasValue ?
                new ObjectParameter("RoleId", roleId) :
                new ObjectParameter("RoleId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteRoles", roleIdParameter);
        }
    
        public virtual ObjectResult<GetAllCategories_Result> GetAllCategories()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllCategories_Result>("GetAllCategories");
        }
    
        public virtual ObjectResult<GetAllCategory_Result> GetAllCategory(Nullable<int> categoryId, string categoryName)
        {
            var categoryIdParameter = categoryId.HasValue ?
                new ObjectParameter("CategoryId", categoryId) :
                new ObjectParameter("CategoryId", typeof(int));
    
            var categoryNameParameter = categoryName != null ?
                new ObjectParameter("CategoryName", categoryName) :
                new ObjectParameter("CategoryName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllCategory_Result>("GetAllCategory", categoryIdParameter, categoryNameParameter);
        }
    
        public virtual ObjectResult<GetAllRoles_Result> GetAllRoles(Nullable<int> rolesID, string rolesName)
        {
            var rolesIDParameter = rolesID.HasValue ?
                new ObjectParameter("RolesID", rolesID) :
                new ObjectParameter("RolesID", typeof(int));
    
            var rolesNameParameter = rolesName != null ?
                new ObjectParameter("RolesName", rolesName) :
                new ObjectParameter("RolesName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAllRoles_Result>("GetAllRoles", rolesIDParameter, rolesNameParameter);
        }
    
        public virtual ObjectResult<GetCategor_Result> GetCategor()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCategor_Result>("GetCategor");
        }
    
        public virtual ObjectResult<GetCategory_Result> GetCategory()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCategory_Result>("GetCategory");
        }
    
        public virtual ObjectResult<GetRoles_Result> GetRoles()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetRoles_Result>("GetRoles");
        }
    
        public virtual ObjectResult<GetUsers_Result> GetUsers(string username, string pass)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("username", username) :
                new ObjectParameter("username", typeof(string));
    
            var passParameter = pass != null ?
                new ObjectParameter("Pass", pass) :
                new ObjectParameter("Pass", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetUsers_Result>("GetUsers", usernameParameter, passParameter);
        }
    
        public virtual int InsertProduct(string productName, Nullable<decimal> price, string imgext, string imagePath, string categoryName, string createdBy)
        {
            var productNameParameter = productName != null ?
                new ObjectParameter("ProductName", productName) :
                new ObjectParameter("ProductName", typeof(string));
    
            var priceParameter = price.HasValue ?
                new ObjectParameter("Price", price) :
                new ObjectParameter("Price", typeof(decimal));
    
            var imgextParameter = imgext != null ?
                new ObjectParameter("imgext", imgext) :
                new ObjectParameter("imgext", typeof(string));
    
            var imagePathParameter = imagePath != null ?
                new ObjectParameter("imagePath", imagePath) :
                new ObjectParameter("imagePath", typeof(string));
    
            var categoryNameParameter = categoryName != null ?
                new ObjectParameter("CategoryName", categoryName) :
                new ObjectParameter("CategoryName", typeof(string));
    
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertProduct", productNameParameter, priceParameter, imgextParameter, imagePathParameter, categoryNameParameter, createdByParameter);
        }
    
        public virtual int InsertUsers(Nullable<int> userId, string name, string username, string pass, string email, string number, Nullable<System.DateTime> createdDate, Nullable<int> roleId)
        {
            var userIdParameter = userId.HasValue ?
                new ObjectParameter("UserId", userId) :
                new ObjectParameter("UserId", typeof(int));
    
            var nameParameter = name != null ?
                new ObjectParameter("Name", name) :
                new ObjectParameter("Name", typeof(string));
    
            var usernameParameter = username != null ?
                new ObjectParameter("username", username) :
                new ObjectParameter("username", typeof(string));
    
            var passParameter = pass != null ?
                new ObjectParameter("Pass", pass) :
                new ObjectParameter("Pass", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var numberParameter = number != null ?
                new ObjectParameter("number", number) :
                new ObjectParameter("number", typeof(string));
    
            var createdDateParameter = createdDate.HasValue ?
                new ObjectParameter("CreatedDate", createdDate) :
                new ObjectParameter("CreatedDate", typeof(System.DateTime));
    
            var roleIdParameter = roleId.HasValue ?
                new ObjectParameter("RoleId", roleId) :
                new ObjectParameter("RoleId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("InsertUsers", userIdParameter, nameParameter, usernameParameter, passParameter, emailParameter, numberParameter, createdDateParameter, roleIdParameter);
        }
    
        public virtual int IUCategory(string categoryName, string createdBy)
        {
            var categoryNameParameter = categoryName != null ?
                new ObjectParameter("CategoryName", categoryName) :
                new ObjectParameter("CategoryName", typeof(string));
    
            var createdByParameter = createdBy != null ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("IUCategory", categoryNameParameter, createdByParameter);
        }
    
        public virtual int IURole(ObjectParameter roleID, string roleName, string mode)
        {
            var roleNameParameter = roleName != null ?
                new ObjectParameter("RoleName", roleName) :
                new ObjectParameter("RoleName", typeof(string));
    
            var modeParameter = mode != null ?
                new ObjectParameter("Mode", mode) :
                new ObjectParameter("Mode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("IURole", roleID, roleNameParameter, modeParameter);
        }
    
        public virtual ObjectResult<string> Login(string username, string pass, Nullable<int> roleId)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("username", username) :
                new ObjectParameter("username", typeof(string));
    
            var passParameter = pass != null ?
                new ObjectParameter("Pass", pass) :
                new ObjectParameter("Pass", typeof(string));
    
            var roleIdParameter = roleId.HasValue ?
                new ObjectParameter("RoleId", roleId) :
                new ObjectParameter("RoleId", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("Login", usernameParameter, passParameter, roleIdParameter);
        }
    
        public virtual int UpdateCategory(Nullable<int> categoryId, string categoryName, Nullable<int> createdBy)
        {
            var categoryIdParameter = categoryId.HasValue ?
                new ObjectParameter("CategoryId", categoryId) :
                new ObjectParameter("CategoryId", typeof(int));
    
            var categoryNameParameter = categoryName != null ?
                new ObjectParameter("CategoryName", categoryName) :
                new ObjectParameter("CategoryName", typeof(string));
    
            var createdByParameter = createdBy.HasValue ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateCategory", categoryIdParameter, categoryNameParameter, createdByParameter);
        }
    }
}
