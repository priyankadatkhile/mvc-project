﻿namespace Assignments.Models
{
    public class InvoiceModel
    {
      
        public string userName { get; set; }

        public string productName { get; set; }

        public string description { get; set; }

        public int? quantity { get; set; }

        public double in_totalbill { get; set; }

        public double? o_bill { get; set; }
    }
}