﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.ComponentModel.DataAnnotations;

namespace Assignments.Models
{
    public class Registration
    {
        public int UserId { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "UserName")]
        public string username { get; set; }

        [Required]
        [Display(Name ="Password")]
        public string Pass { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Phone Number")]
        public string number { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        //public Nullable<int> RoleId { get; set; }

        public virtual Role Role { get; set; }


        public DataSet GetRoles { get; set; }

        public int RoleID { get; set; }

        public string RoleName { get; set; }
    }
}