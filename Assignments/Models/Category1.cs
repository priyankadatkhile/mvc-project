﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Assignments.Models
{
    public class Category1
    {
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Category Name is Required.")]
        public string CategoryName { get; set; }

        [Required(ErrorMessage = "Created By is Required.")]
        public string CreatedBy { get; set; }
    }
}