﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignments.Models
{
    public class cart
    {
        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public float ProductPrice { get; set; }
        public int qty { get; set; }
        public float bill { get; set; }
    }
}