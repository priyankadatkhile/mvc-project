﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Assignments.Models
{
    public class Role1
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public DataSet StoreAllRoles { get; set; }

        public IList<SelectListItem> Drp_RoleNames { get; set; }

        public SelectList MobileList { get; set; }

        public DataSet GetRoles { get; set; }
    }
}