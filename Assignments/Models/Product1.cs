﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Data;
using System.Web.Mvc;

namespace Assignments.Models
{
    public class Product1
    {
        public int ProductId { get; set; }
        //public Nullable<int> CategoryId { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public string Description { get; set; }
        public string ProductImage { get; set; }
        public Nullable<int> CreatedBy { get; set; }

        public DataSet GetCategory { get; set; }
        public int UserId { get; set; }
        public IEnumerable<SelectListItem> CategorySelectListItem { get; set; }

        [DisplayName("Upload File")]
        //public string imagePath { get; set; }

        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        //public string CreatedBy { get; set; }

        //public HttpPostedFileBase ImageFile { get; set; }
    }
}