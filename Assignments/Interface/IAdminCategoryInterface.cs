﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments.Interface
{
    public interface IAdminCategoryInterface
    {
        void GetConnString();

        void Add_Category(Models.Category category);

        DataSet Show_record_byid(int categoryId);

        void Update_Category(Models.Category cat);

        void Delete_Category(int id);

        void Pagination(string sortOn, string orderBy, string pSortOn, string keyword, int? page);
    }
}
