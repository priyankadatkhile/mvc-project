﻿using Assignments.Models;
using System.Collections.Generic;

namespace Assignments.Interface
{
    public interface Iinvoice
    {
        List<InvoiceModel> Getinvoice();
        int GetCurrentUserId();
    }
}
