﻿using System.Data;

namespace Assignments.Interface
{
    public interface IRole
    {
        void GetConnString();
        DataSet Show_Data();

        void Add_Role(Models.Role role);

        void update_Role(Models.Role role);

        DataSet Show_role_byid(int RoleId);

        void Delete_Role(int RoleId);
    }
}
