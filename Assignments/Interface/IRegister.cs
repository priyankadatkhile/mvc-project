﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Assignments.Models;

namespace Assignments.Interface
{
    public interface IRegister
    { 
        void GetConnString();

        DataSet GetAllRols();

        void Register(Registration registration);

        DataSet Login(Login user);
    }
}