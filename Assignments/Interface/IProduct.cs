﻿using Assignments.Models;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Mvc;

namespace Assignments.Interface
{
    public interface IProduct
    {
        DataSet GetAllCategory();
        void GetConnString();
        string Create(HttpPostedFileBase file, Models.Product product);
        List<Category1> SelectallCategories();

        string Edit(HttpPostedFileBase file, Models.Product product);

        string Delete(int? id);
    }
}
