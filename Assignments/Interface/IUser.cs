﻿using Assignments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignments.Interface
{
    public interface IUser
    {
        Product AddToCart(int? Id);
    }
}
